刚好在学习Android开发, 回答下这个问题测试一下我的学习成果

1. Servlet的名字取得很奇葩,很少见用数字123做为servlet的名字的, 代码的可读性大打折扣.

2. localhost指向的是手机本身或是模拟器本身,你的tomcat服务肯定不是部署在模拟器或手机上,
如果你是用的模拟器, 则必须把localhost改成10.0.2.2

3. 当你的tomcat服务启动后,不要急于在手机上做测试, 
你应该在浏览器上打开`http://localhost:8080/HttpWebServerDemo1/123?name=cyper&age=20`看看响应是否正常.
因为你的servlet中有中文,至少你得加上如下代码:

        response.addHeader("Content-Type","text/plain;charset=UTF-8");

4. 修改后的servlet代码如下:

        String name = request.getParameter("name");
        String age = request.getParameter("age");
        response.addHeader("Content-Type","text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("使用Get方法发送请求，我的名字是" + name + "我的年龄是" + age);

5. 从3.0开始,在UI线程中做network i/o是不允许的,参考[so][],
你必须使用new Thread + Handler或AsynTask, 否则会报android.os.NetworkOnMainThreadException

6. 你必须在manifest文件中声明使用INTERNET的权限.

        <uses-permission android:name="android.permission.INTERNET" />

    否则会报java.net.SocketException: socket failed: EACCES (Permission denied)

7. 完整代码见[git osc][]

[so]:http://stackoverflow.com/questions/6343166/android-os-networkonmainthreadexception
[git osc]:https://git.oschina.net/uniquejava/AndroidNetworkTest.git